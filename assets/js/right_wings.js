﻿
$(function () {

    var $sidebar = $("#sky-wrap"),
        $window = $(window),
        offset = $sidebar.offset(),
        topPadding = 30;
    var footer_of = $("footer").offset().top;
    var footer_value = $("footer").offset().top - $("#sky-wrap").height();

    $window.scroll(function () {

        if ($(this).scrollTop() == 0) {

            $sidebar.stop().animate({ marginTop: 0 });
            $sidebar.stop().css("top", "");
        }
        else {
            if ($window.scrollTop() < offset.top) {
                $sidebar.stop().animate({
                    top: 100
                    // marginTop: -260
                });
            }

            else {

                if ($window.scrollTop() >= (parseInt(footer_value) - 350)) {

                    $sidebar.stop().animate({
                        top: 0
                    });
                }
                else {
                    $sidebar.stop().animate({
                        top: 100
                    });
                }
            }
        }
    });

    if ($(this).scrollTop() == 0) {

        $sidebar.stop().animate({
            marginTop: 0
        });

        $sidebar.stop().css("top", "");
    }
});
